#ifndef SRC_BCMATHSH
#define SRC_BCMATHSH
#include <cmath>
#include <string>

/**
 * @file bcmaths.h
 * @date 2020-02-18
 
 * @author Daniel Sedlacek (xsedla1p)
 * @author Dominik Kroupa (xkroup12)
 * @author Roland Schulz (xschul06)
 * @author Rati Kereselidze (xkeres01)
 
 * @brief Bublinky Calculator MATHS library header file
 */

// Constants

const double PI = 3.14159265358979323846;

// Functions

/** 
 * @brief sine
 *  * trigonometric function, returns the y-axis of given angle
 *
 * @param [in] arg1 angle in degrees
 *
 * @returns y-axis of arg1 angle 
 */
double sine(double arg1);

/** 
 * @brief cosine
 *  * trigonometric function, returns the y-axis of given angle
 *
 * @param [in] arg1 angle in degrees
 *
 * @returns y-axis of arg1 angle
 */
double cosine(double arg1);

/** 
 * @brief power of a number
 *  * mathematical operation, calculates the power/exponent of a number,
 *  * if base is negative, exponent can only be integer
 *
 * @param [in] arg1 base value
 * @param [in] arg2 power/exponent value
 *
 * @returns arg1^arg2
 */
double power(double arg1, double arg2);

/** 
 * @brief root
 *  * mathematical operation, calculates nth root of a number,
 *  * root from negative base cannot be calculated
 *
 * @param [in] arg1 nth root
 * @param [in] arg2 base value
 *
 * @returns nth root (arg1) of a number (arg2)
 */
double root(double arg1, double arg2);

/** 
 * @brief factorial
 *  * mathematical operation, calculates the multiplication of all numbers
 *  starting from given number down to 1 
 *  * negative or real factorial cannot be calculated 
 *
 * @param [in] arg1 number where factorial starts
 *
 * @returns arg1!
 */
double fact(double arg1);

/** 
 * @brief minus
 *  * basic arithmetic operation, calculates the subtraction of two numbers
 *
 * @param [in] arg1 first number
 * @param [in] arg2 number to be subtracted
 *
 * @returns arg1-arg2
 */
double minus(double arg1, double arg2);

/** 
 * @brief addition
 *  * basic arithmetic operation, calculates the addition of two numbers
 *
 * @param [in] arg1 first number
 * @param [in] arg2 number to be added
 *
 * @returns arg1+arg2
 */
double plus(double arg1, double arg2);

/** 
 * @brief absolute value
 *
 * @param [in] arg1 number for absolute value
 *
 * @returns |arg1|
 */
double absolute(double arg1);

/** 
 * @brief division
 *  * basic arithmetic operation, divides two numbers,
 *  * divisor cannot be zero
 *
 * @param [in] arg1 dividend
 * @param [in] arg2 divisor
 *
 * @returns arg1/arg2
 */
double division(double arg1, double arg2);

/** 
 * @brief multiplication
 *  * basic arithmetic operation, multiplicates two numbers
 *
 * @param [in] arg1 number to be multiplicated
 * @param [in] arg2 number to multiplicate arg2
 *
 * @returns arg1*arg2
 */
double multiplication(double arg1, double arg2);

#endif
