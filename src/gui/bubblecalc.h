#ifndef BUBBLECALC_H
#define BUBBLECALC_H

#include <QDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class bubblecalc; }
QT_END_NAMESPACE

class bubblecalc : public QDialog
{
    Q_OBJECT

public:
    bubblecalc(QWidget *parent = nullptr);
    ~bubblecalc();

private:
    Ui::bubblecalc *ui;
    void load_vals();
    void math_operation_falser();
    void error_message(QString* displayVal);
    double calculate();

private slots:
    void num_pressed();
    void clear();
    void insert();
    void del();
    void floating_point();
    void save();
    void operator_pressed();
    void function_pressed();
    void equal_button();
    void open_help();
};
#endif // BUBBLECALC_H
