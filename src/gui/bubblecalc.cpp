#include "bubblecalc.h"
#include "./ui_bubblecalc.h"
#include "../bcmaths.cpp"
#include "QUrl"
#include "QDesktopServices"

/**  
 * @file bubblecalc.cpp  
 * @date 2020-02-18   
 * @author Daniel Sedlacek (xsedla1p)  
 * @brief Bublinky Calculator GUI file  
 */

double firstVal; // Stores value before operator
double secondVal; // Stores value after operator
double saved = 0.0; // Stores saved value
double ans = 0.0; // Stores value of the last result

// Defines what operation was chosen, if any
bool divideOperation = false;
bool multiplyOperation = false;
bool addOperation = false;
bool subOperation = false;
bool powerOperation = false;
bool sqrtOperation = false;
bool operation = false;
bool floatingPointOp = false;
bool floatingPointNoOp = false;

// Constructor
bubblecalc::bubblecalc(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::bubblecalc)
{

    ui->setupUi(this);


    ui->Del->setIcon(QIcon("../src/gui/icons/delete.ico"));
    ui->Del->setIconSize(QSize(20,20));

    ui->Insert->setToolTip("0");
    ui->Ans->setToolTip("0");

    // Stores all numerical buttons
    QPushButton *numButtons[10];

    for (int i = 0; i < 10; ++i){
        QString butName = "Button" + QString::number(i);
        numButtons[i] = bubblecalc::findChild<QPushButton *>(butName);

        connect(numButtons[i], SIGNAL(released()), this, SLOT(num_pressed()));
    }

    // Stores all operation buttons
    QPushButton *operatorButtons[6];
    QPushButton *funButtons[4];
    char operatorName[6][10] = {{"Multiply"},{"Minus"},{"Divide"},{"Sqr"},{"Plus"},{"Sqrt"}};
    char funName[4][10] = {{"Factorial"},{"Abs"},{"Cos"},{"Sin"}};

    for (int i = 0; i <= 6; ++i){
        operatorButtons[i] = bubblecalc::findChild<QPushButton *>(operatorName[i]);
        connect(operatorButtons[i], SIGNAL(released()), this, SLOT(operator_pressed()));
    }

    for (int i = 0; i <= 4; ++i){
        funButtons[i] = bubblecalc::findChild<QPushButton *>(funName[i]);
        connect(funButtons[i], SIGNAL(released()), this, SLOT(function_pressed()));

    }

    connect(ui->Pi, SIGNAL(released()), this, SLOT(insert()));
    connect(ui->Ans, SIGNAL(released()), this, SLOT(insert()));
    connect(ui->Del, SIGNAL(released()), this, SLOT(del()));
    connect(ui->Save, SIGNAL(released()), this, SLOT(save()));
    connect(ui->Clear, SIGNAL(released()), this, SLOT(clear()));
    connect(ui->Equals, SIGNAL(released()), this, SLOT(equal_button()));
    connect(ui->Insert, SIGNAL(released()), this, SLOT(insert()));
    connect(ui->FloatingPoint, SIGNAL(released()), this, SLOT(floating_point()));
    connect(ui->Help, SIGNAL(released()), this, SLOT(open_help()));
}

// Destructor
bubblecalc::~bubblecalc(){
    delete ui;
}

/**
 * @brief num pressed
 *  * displays numerical value of pressed button on display
 */
void bubblecalc::num_pressed(){
    QPushButton *button = (QPushButton *)sender();
    QString butVal = button->text();
    QString displayVal = ui->Display->text();
    error_message(&displayVal);
    ui->Display->setText(displayVal + butVal);
}

/**
 * @brief operator pressed
 *  * displays operator from pressed button on display
 *  * determines which math operation was selected
 */
void bubblecalc::operator_pressed(){
    QPushButton *button = (QPushButton *)sender();
    QString operatorPressed = button->text();
    QString displayVal = ui->Display->text();
    error_message(&displayVal);

    if (operation == false && displayVal == "" && operatorPressed == "-") {
        ui->Display->setText(operatorPressed);
    } else if (operation == true && displayVal[displayVal.length() - 1] == " " && operatorPressed == "-"){    
        ui->Display->setText(displayVal + operatorPressed);
    } else {
       if (operation == false) {
           ui->Display->setText(displayVal + " " + operatorPressed + " ");
           if (operatorPressed == "+") {
               addOperation = true;
           } else if (operatorPressed == "-"){
               subOperation = true;
           } else if (operatorPressed == "/"){
               divideOperation = true;
           } else if (operatorPressed == "*"){
               multiplyOperation = true;
           } else if (operatorPressed == "^"){
               powerOperation = true;
           } else if (operatorPressed == "√"){
               sqrtOperation = true;
           }

           operation = true;
      }
    }
}

/**
 * @brief function pressed
 *  * applies pressed function on last entered number
 */
void bubblecalc::function_pressed(){
    load_vals();

    QString displayVal = ui->Display->text();

    if (displayVal != "" && displayVal != "Error"){
        QPushButton *button = (QPushButton *)sender();
        QString funPressed = button->objectName();
        QString operators = "+-*/^√";
        QString firstDisplayVal;
        int position = 0;
        double currentVal;
        const int signPlusSpace = 2;


        if (operation == true){
            currentVal = secondVal;
        } else {
            currentVal = firstVal;
        }

        if (funPressed == "Cos") {
            currentVal = cosine(currentVal);
        } else if (funPressed == "Sin") {
            currentVal = sine(currentVal);
        } else if (funPressed == "Abs") {
            currentVal = absolute(currentVal);
        } else if (funPressed == "Factorial") {
            currentVal = fact(currentVal);
        }

	// If no operation was selected, rewrite display value with calculated value
        if (!operation){
            ui->Display->setText(QString::number(currentVal));
	// Else rewrite only the part after an operator with calculated value
        } else {
	    // Finds the position of operator
            while (!operators.contains(displayVal[position]) || displayVal[position + 1] != " " ){
                position++;
            }
            firstDisplayVal = displayVal;
	    // Extracts the first value and operator
            firstDisplayVal.remove(position + signPlusSpace, displayVal.length());
            // Displays the whole expression with calculated second value
            ui->Display->setText(firstDisplayVal + QString::number(currentVal));
        }
    }
}

/**
 * @brief clear
 *  * clears display
 */
void bubblecalc::clear(){
    ui->Display->setText("");

    if (operation == true){
        math_operation_falser();
    }

    floatingPointNoOp = false;
}

/**
 * @brief insert
 *  * inserts values depending on the button pressed
 *  * inserts either from saved, ans, or PI
 */
void bubblecalc::insert(){
    QPushButton *button = (QPushButton *)sender();
    QString butName = button->objectName();
    QString displayVal = ui->Display->text();
    error_message(&displayVal);

    // Determines which button was pressed and inserts the value
    if (butName == "Insert"){
        ui->Display->setText(displayVal + QString::number(saved));
    } else if (butName == "Ans"){
        ui->Display->setText(displayVal + QString::number(ans));
    } else if (butName == "Pi"){
        ui->Display->setText(displayVal + QString::number(PI));
    }
}

/**
 * @brief del
 *  * deletes values from display
 *  * numbers are being deleted one by one
 *  * words are being deleted as a whole
 */
void bubblecalc::del(){
    QString displayVal = ui->Display->text();
    error_message(&displayVal);

    if (displayVal != ""){

        QString lastChar = displayVal.at(displayVal.length() - 1);
	// Determines if floating point was before or after operator
	// Sets the FloatingPoint variable to false, so that the user can use it again
        if (lastChar == '.'){
             if (floatingPointNoOp == true && operation == false){
                 floatingPointNoOp = false;
             }

             if (floatingPointOp == true && operation == true){
                 floatingPointOp = false;
             }
         }
	
	// Deletes operator with spaces
        if (lastChar == ' '){
             math_operation_falser();
             for (int i = 0; i < 3; i++ )
                 displayVal.remove(displayVal.length()-1,1);
	 // Deletes whole inf or nan
	 } else if (lastChar == 'f' || lastChar == 'n'){
             for (int i = 0; i < 3; i++ )
                 displayVal.remove(displayVal.length()-1,1);
	 // Deletes one character
	 } else {
             displayVal.remove(displayVal.length()-1,1);
         }
		
         ui->Display->setText(displayVal);
    }
}

/**
 * @brief floating point
 *  * determines when to insert a floating point or not
 */
void bubblecalc::floating_point(){
    QString displayVal = ui->Display->text();
    error_message(&displayVal);

    if (displayVal == "") {
        ui->Display->setText(displayVal + "0.");
        floatingPointNoOp = true;
    } else if (displayVal.at(displayVal.length() - 1).isDigit()){
        if (floatingPointNoOp == false && operation == false){
            ui->Display->setText(displayVal + ".");
            floatingPointNoOp = true;
        } else if (floatingPointOp == false && operation == true){
            ui->Display->setText(displayVal + ".");
            floatingPointOp = true;
        }
    }
}

/**
 * @brief load values
 *  * loads values from display
 */
void bubblecalc::load_vals(){ 
    QString displayVal = ui->Display->text();
    if (displayVal == "Error" || displayVal == "nan"){
    	return;
    }

    if (displayVal != ""){
        QString operators = "+-*/^√";
        QString tmpVal = displayVal;
        QString strFirstVal;
        QString strSecondVal;
        int numOfOperators = 0;
        int position = 0;
        // Case of value without operator being pressed, except for first occurence of "-".
        if (!operation){
	    // Check the number of floating points and minus signs
            if (tmpVal.count('.') > 1 || tmpVal.count('-') > 1 || (tmpVal.count('-') > 0 && tmpVal[0] != '-')){
                ui->Display->setText("Error");
            }
            // Check for an occurence of lonely sign
       	    if (displayVal[0] == "-" && displayVal.length() == 1){
                ui->Display->setText("Error");
                return;
            }

	    firstVal = displayVal.toDouble();
	// Case of expression with an operator
        } else {
	    // Check if first value is an operator. All operators start with space. Minus sign is permitted.
            if (displayVal[0] == " "){
		ui->Display->setText("Error");
	    	return;
	    }
	    // Checks for lonely minus sign
	    if(displayVal[0] == "-" && displayVal[1] == " "  || displayVal[displayVal.length() - 1] == "-"){
	    	ui->Display->setText("Error");
		return;
	    }
            // Skips the first position when it's "-"
            if (displayVal[0] == "-"){
                position = 1;
            }
            // Search for the first appearance of an operator
            while (!operators.contains(displayVal[position])){
                position++;
            }
            // Extract first value from display and check for number of floating points
            tmpVal.remove(position, displayVal.length());
            if (tmpVal.count('.') > 1){
                ui->Display->setText("Error");
            } else {
                firstVal = tmpVal.toDouble();
            }

            tmpVal = displayVal;
            // Extract second value from display
	    tmpVal.remove(0, position + 1);
            // Check the number of operators
            for (int i = 0; i < tmpVal.length(); i++){
                if (operators.contains(tmpVal[i])){
                    numOfOperators++;
                }
            }
            // Check if expression is valid
            if (tmpVal.count('.') > 1 || numOfOperators > 1 || (numOfOperators > 0 && tmpVal[1] != '-') || tmpVal == " "){
                ui->Display->setText("Error");
            } else {
                secondVal = tmpVal.toDouble();
            }
        }
    }
}

/**
 * @brief equal button
 *  * displays calculated value
 *  * saves calculated value to ans
 */
void bubblecalc::equal_button(){  
    load_vals();
    QString displayVal = ui->Display->text();
    if (!displayVal.contains("nan") && displayVal != "Error" && displayVal != ""){
        ans = calculate();
        math_operation_falser();
	ui->Ans->setToolTip(QString::number(ans));
	ui->Display->setText(QString::number(ans));
    	displayVal = ui->Display->text();
	if (!displayVal.contains('.')){
	    floatingPointOp = false;
	}
    } else if (displayVal.contains("nan")) {
    	ui->Display->setText("Error");
    }

}

/**
 * @brief save
 *  * saves the calculated value from displayed expression
 */
void bubblecalc::save(){
    load_vals();
    QString displayVal = ui->Display->text();
    if (!displayVal.contains("nan") && displayVal != "Error" && displayVal != ""){
	saved = calculate();
        ui->Insert->setToolTip(QString::number(saved));
    } 
}

/**
 * @brief calculate
 *  * calculates the displayed expression
 *
 *  @returns calculated value
 */
double bubblecalc::calculate(){	
    QString displayVal = ui->Display->text();
        if (operation){
            if (addOperation){
                return plus(firstVal, secondVal);
            } else if (subOperation) {
                return minus(firstVal,secondVal);
            } else if (multiplyOperation) {
                return multiplication(firstVal,secondVal);
            } else if (divideOperation) {
                return division(firstVal,secondVal);
            } else if (powerOperation) {
                return power(firstVal,secondVal);
            } else if (sqrtOperation) {
                return root(firstVal,secondVal);
            }

        } else if (!operation && displayVal != "") {
            return displayVal.toDouble();
        }
    return 0;
}

/**
 * @brief operation falser
 *  * sets all operations to false
 */
void bubblecalc::math_operation_falser(){
    divideOperation = false;
    multiplyOperation = false;
    addOperation = false;
    subOperation = false;
    powerOperation = false;
    sqrtOperation = false;
    operation = false;
    floatingPointOp = false;
    floatingPointNoOp = false;

}

/**
 * @brief error message
 *  * checks for "Error" or "nan" on display and clears it
 */
void bubblecalc::error_message(QString* displayVal){
    if (*displayVal == "Error" || *displayVal == "nan") {
        ui->Display->setText("");
        *displayVal = "";
        math_operation_falser();
    }
}

/**
 * @brief open help
 *  * opens external help file
 */
void bubblecalc::open_help(){
    QString applicationPath = QCoreApplication::applicationFilePath();
    applicationPath.remove(applicationPath.length() - 17, 17);
    QDesktopServices::openUrl(QUrl::fromLocalFile(applicationPath + "/src/help.txt"));
}
