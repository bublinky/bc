#include "bubblecalc.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    bubblecalc w;
    w.show();
    return a.exec();
}
