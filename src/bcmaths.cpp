#include "bcmaths.h"

/**
 * @file bcmaths.cpp
 * @date 2020-02-18
 
 * @author Daniel Sedlacek (xsedla1p)
 * @author Dominik Kroupa (xkroup12)
 * @author Roland Schulz (xschul06)
 * @author Rati Kereselidze (xkeres01)
 
 * @brief Bublinky Calculator MATHS library functions file
 */

/** 
 * @brief 
 *
 * @param [in] arg1
 *
 * @returns 
 *
 * @pre 
 * @post
 */
double sine(double arg1)
{
    //sub 90° from arg1 to shift the sin x-axis
    //to be able to use cosine() for computation
    arg1 = minus(arg1, 90);
    //call cosine() to calculate result
    return cosine(arg1);
}

/** 
 * @brief 
 *
 * @param [in] arg1
 *
 * @returns 
 *
 * @pre 
 * @post
 */
double cosine(double arg1)
{
    int cos_sign;
    // sign -1 because taylor series always starts with minus
    int sign = -1;
    //tmp_result to store first part of calculating taylor formula
    double tmp_result;
    double result = 1;
    //used in for cycle, if arg1 is too big, add/sub more than 360°
    double big_val_1b = multiplication(360, power(10, 17));
    double big_val_2b = multiplication(360, power(10, 12));
    double big_val_3b = multiplication(360, power(10, 8));
    double big_val_4b = multiplication(360, power(10, 4));
    //to prevent missing desired interval while subbing/adding values
    //arg1 has to have twice the size
    double big_val_1a = multiplication(big_val_1b, 2);
    double big_val_2a = multiplication(big_val_2b, 2);
    double big_val_3a = multiplication(big_val_3b, 2);
    double big_val_4a = multiplication(big_val_4b, 2);

    //catching problematic values
    if (arg1 == INFINITY) return NAN;
    
    //used in for cycle, helps to determine to add/sub 360°
    if (arg1 >= 0) cos_sign = -1;
        else cos_sign = +1;

    for (int i = 0; arg1 < -180 || arg1 > 180; i++) {
        //add or sub 360° if not in <-180,180>

        //if value of arg1 is too big, add or sub big value
        //this is done to prevent long cycles

        if (arg1 > big_val_1a) {
            arg1 = plus(arg1, multiplication(cos_sign, big_val_1b));
        }
        else if (arg1 > big_val_2a){
            arg1 = plus(arg1, multiplication(cos_sign, big_val_2b));
        }
        else if (arg1 > big_val_3a) {
            arg1 = plus(arg1, multiplication(cos_sign, big_val_3b));
        }
        else if (arg1 > big_val_4a) {
            arg1 = plus(arg1, multiplication(cos_sign, big_val_4b));
        }
        //arg1=arg1+(cos_sign*360); 
        else arg1 = plus(arg1, multiplication(cos_sign, 360));


    }

    //convert degree value to radians
    arg1 = (arg1 * PI) / 180.00;

    //use taylor series approximation to calculate cos value
    for(int number = 2; number <= 22; number +=2) {
        //tmp_result= (pow(arg1, number) ) / (fact(number) )
        tmp_result=division(pow(arg1, number), fact(number)); 
        //result+=sign*tmp_result
        result = plus(result, multiplication(sign, tmp_result));
        sign *= -1;
    }

    return result;
}

/** 
 * @brief 
 *
 * @param [in] arg1
 * @param [in] arg2
 *
 * @returns 
 *
 * @pre 
 * @post
 */
double power(double arg1, double arg2)
{
    //to store integer and fractional parts of exponent
    double int_part_exp;
    double fract_part_exp;
    //to set whether exp is integer or not
    bool exp_r = 0;
    //to store final result
    double result = 0;
    //to store results of int and fract part of exponent
    double result_i = 1;
    double result_f = 1;
    //to store how many decimals fract part contains
    int divisor = 1;
    bool length_count = 0;
    std::string fraction_count;
    std::string::iterator i;
    //needed to convert double to str, stores int part
    double str_helper;

    //solving problematic values
    if (arg2 == 0){
        if (arg1 == INFINITY) return NAN;
            else return 1;
    }
    if (std::isnan(arg1) || std::isnan(arg2)) {
        return NAN;
    }
    if (arg2 == INFINITY){
        if (arg1 > 0) return INFINITY;
        if (arg1 < 0) return -INFINITY;
            else return 0;
    }

    //get int part and fract part from double
    fract_part_exp=std::modf(arg2, &int_part_exp);

    //solving problematic values with real exponent
    if (arg1 < 0 && fract_part_exp > 0) {
        return NAN;
    }

    //set boolean values if fract part !=0
    if (fract_part_exp != 0.0) exp_r = 1;

    //calculate result with integer part of exponent
    for (int i = 0; i < fabs(int_part_exp); i++) {
        //result_i*=arg1;
        result_i = multiplication(result_i, arg1);
        //if condition to prevent long cycling
        if (result_i == INFINITY) return INFINITY;
    }

    //calculate result with fractional part of exponent
    if (exp_r) {
        //convert exponent to string
        fraction_count = std::to_string(modf(arg2,&str_helper));

        //count how many numbers we have in fract part
        i = fraction_count.end(); //initialization for 'for' cycle
        for (; ! (*i == '.' || *i == ','); i--) {
            if (! ( *i == '0' || *i == '\0')) length_count = 1;
            if (length_count) {
                divisor = multiplication(divisor, 10); /*divisor*=10*/
                if (divisor == 1000) break; //10^3, 3=max precision
            }
        }
        //multiply fractional part to create integer and calculate result
        //fract_part_exp*=divisor;
        fract_part_exp = multiplication(fract_part_exp, divisor);
        fract_part_exp = std::modf(fract_part_exp, &int_part_exp);
        //result_f=pow(arg1,fract_part_exp/divisor);
        result_f = root(divisor, power(arg1, int_part_exp));
    }

    //result=result_i*result_f;
    result = multiplication(result_i, result_f);
    //base^-exponent = 1/base^exponent
    if (arg2 < 0.0) result = division(1, result); /*result=1/result*/
    return result;

}

/** 
 * @brief 
 *
 * @param [in] arg1
 * @param [in] arg2
 *
 * @returns 
 *
 * @pre 
 * @post
 */
double root(double arg1, double arg2)
{
    double epsilon = 1e-20;
    double difference = 0;
    double guess = 0;
    double prev_guess = 1;
    double power_of_guess;
    double lowest = 0;
    double highest = arg2;

    //solving problematic values
    if (arg2 < 0) return NAN;
    if (arg1 == 0) return NAN;
    if (std::isnan(arg1) || std::isnan(arg2)) return NAN;
    if (arg1 == INFINITY && arg2 == INFINITY) return 1;

    //using division algorithm to approximate result
    while (absolute(minus(prev_guess, guess)) >= epsilon) {
        //decide which side of algorithm to throw away
        if (difference > 0) {
            if (arg1 > 0) highest = guess;
            if (arg1 < 0) lowest = guess;
        }
        else if (difference < 0) {
            if (arg1 > 0) lowest = guess;
            if (arg1 < 0) highest = guess;
        }
        prev_guess = guess;
        //guess=(lowest+highest)/2
        guess = division(plus(lowest, highest), 2);
        power_of_guess = power(guess, arg1);
        difference = minus(power_of_guess, arg2);
    }
    return guess;

}

/** 
 * @brief 
 *
 * @param [in] arg1
 *
 * @returns 
 *
 * @pre 
 * @post
 */
double fact(double arg1)
{
    double int_helper;
    if (std::modf(arg1, &int_helper) != 0.0 || arg1 < 0 ) return NAN;
    if (arg1 == 0) return 1;
    if (arg1 < 0) return NAN;
    for (int i = arg1-1; i >= 1; i--) {
        arg1 = arg1*i;
    }
    return arg1;
}

/** 
 * @brief 
 *
 * @param [in] arg1
 * @param [in] arg2
 *
 * @returns 
 *
 * @pre 
 * @post
 */
double minus(double arg1, double arg2)
{
    return arg1-arg2;
}

/** 
 * @brief 
 *
 * @param [in] arg1
 * @param [in] arg2
 *
 * @returns 
 *
 * @pre 
 * @post
 */
double plus(double arg1, double arg2)
{
    return arg1+arg2;
}

/** 
 * @brief 
 *
 * @param [in] arg1
 *
 * @returns 
 *
 * @pre 
 * @post
 */
double absolute(double arg1)
{
    if (arg1 < 0) {
        return multiplication(arg1, -1);
    }
    return arg1;
}

/** 
 * @brief 
 *
 * @param [in] arg1
 * @param [in] arg2
 *
 * @returns 
 *
 * @pre 
 * @post
 */
double division(double arg1, double arg2)
{
    if (arg2==0) {
        return NAN;
    }
    else return arg1/arg2;
}

/** 
 * @brief 
 *
 * @param [in] arg1
 * @param [in] arg2
 *
 * @returns 
 *
 * @pre 
 * @post
 */
double multiplication(double arg1, double arg2)
{
    return arg1*arg2;
}
