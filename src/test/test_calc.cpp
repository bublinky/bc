#include<iostream>

#include "bcmaths.h"
#include "gtest/gtest.h"

////////TESTS FOR ABSOLUTE FUNCTION - START////////
TEST (absolute, infinity) {
    //absolute value with infinity returns infinity
    EXPECT_DOUBLE_EQ(absolute(INFINITY), INFINITY);
    //absolute value with -infinity returns +infinity
    EXPECT_DOUBLE_EQ(absolute(-INFINITY), INFINITY);
}
TEST (absolute, not_a_number) {
    //absolute value with 'not a number' returns 'not a number'
    EXPECT_TRUE(std::isnan(absolute(NAN)));
}
TEST (absolute, positive_values) {
    //absolute value with positive numbers returns the same value
    EXPECT_DOUBLE_EQ(absolute(0), 0);
    EXPECT_DOUBLE_EQ(absolute(5), 5);
    EXPECT_DOUBLE_EQ(absolute(100), 100);
}
TEST (absolute, negative_values) {
    //absolute value with negative numbers returns same value but positive
    EXPECT_DOUBLE_EQ(absolute(-5), 5);
    EXPECT_DOUBLE_EQ(absolute(-100), 100);
    EXPECT_DOUBLE_EQ(absolute(-8000), 8000);
}
////////TESTS FOR ABSOLUTE FUNCTION - END////////
//
//
////////TESTS FOR FACT FUNCTION - START////////
TEST (fact, zero_fact) {
    //factorial of 0 is 1
    EXPECT_DOUBLE_EQ(fact(0), 1);
}
TEST (fact, negative_fact) {
    //factorial of negative number is undefined
    EXPECT_TRUE(std::isnan(fact(-1)));
    EXPECT_TRUE(std::isnan(fact(-5)));
}
TEST (fact, real_fact) {
    //factorial of a real number, either positive or negative is undefined
    EXPECT_TRUE(std::isnan(fact(2.1)));
    EXPECT_TRUE(std::isnan(fact(-2.1)));
}
TEST (fact, positive_fact) {
    //factorial of positive number can be computed
    EXPECT_DOUBLE_EQ(fact(1), 1);
    EXPECT_DOUBLE_EQ(fact(3), 6);
    EXPECT_DOUBLE_EQ(fact(5), 120);
    EXPECT_DOUBLE_EQ(fact(10), 3628800);
}
TEST (fact, infinite_fact) {
    //factorial of infinity number is infinity
    EXPECT_DOUBLE_EQ(fact(INFINITY), INFINITY);
}
TEST (fact, nan_fact) {
    //factorial of 'not a number' is not a number
    EXPECT_TRUE(std::isnan(fact(NAN)));
}
////////TESTS FOR FACT FUNCTION - END////////
//
//
////////TESTS FOR MINUS FUNCTION - START////////
TEST (minus, zero) {
    //zero is neutral with subtraction, does not affect the value
    EXPECT_DOUBLE_EQ(minus(5, 0), 5);
    EXPECT_DOUBLE_EQ(minus(0, 5), -5);
    EXPECT_DOUBLE_EQ(minus(0, 0), 0);
}
TEST (minus, infinity) {
    //anything minus infinity equals -infinity
    EXPECT_DOUBLE_EQ(minus(10, INFINITY), -INFINITY);
    //infinity minus anything equals infinity
    EXPECT_DOUBLE_EQ(minus(INFINITY, 15), INFINITY);
    //infinity minus infinity is not defined
    EXPECT_TRUE(std::isnan(minus(INFINITY, INFINITY)));
}
TEST (minus, not_a_number) {
    //anything minus 'not a number' is 'not a number'
    EXPECT_TRUE(std::isnan(minus(NAN, 5)));
    EXPECT_TRUE(std::isnan(minus(10, NAN)));
}
TEST (minus, positive_numbers) {
    //subtraction with positive numbers     
    EXPECT_DOUBLE_EQ(minus(5, 5), 0);
    EXPECT_DOUBLE_EQ(minus(10, 5), 5);
    EXPECT_DOUBLE_EQ(minus(30, 10), 20);
}
TEST (minus, negative_numbers) {
    //subtraction with negative numbers ( - and - is +)
    EXPECT_DOUBLE_EQ(minus(-5, 5), -10);
    EXPECT_DOUBLE_EQ(minus(10, -5), 15);
    EXPECT_DOUBLE_EQ(minus(30, -10), 40);
}
////////TESTS FOR MINUS FUNCTION - END////////
//
//
////////TESTS FOR PLUS FUNCTION - START////////
TEST (plus, zero) {
    //zero is neutral with addition, does not affect the value
    EXPECT_DOUBLE_EQ(plus(5, 0), 5);
    EXPECT_DOUBLE_EQ(plus(0, 5), 5);
    EXPECT_DOUBLE_EQ(plus(0, 0), 0);
}
TEST (plus, INFINITYinite) {
    //anything plus infinity equals infinity
    EXPECT_DOUBLE_EQ(plus(10, INFINITY), INFINITY);
    EXPECT_DOUBLE_EQ(plus(INFINITY, 15), INFINITY);
}
TEST (plus, not_a_number) {
    //anything plus 'not a number' is 'not a number'
    EXPECT_TRUE(std::isnan(plus(NAN, 5)));
    EXPECT_TRUE(std::isnan(plus(10, NAN)));
}
TEST (plus, positive_numbers) {
    //addition with positive numbers        
    EXPECT_DOUBLE_EQ(plus(5, 5), 10);
    EXPECT_DOUBLE_EQ(plus(10, 5), 15);
    EXPECT_DOUBLE_EQ(plus(10, 10), 20);
}
TEST (plus, negative_numbers) {
    //addition with negative numbers        
    EXPECT_DOUBLE_EQ(plus(-5, 5), 0);
    EXPECT_DOUBLE_EQ(plus(10, -5), 5);
    EXPECT_DOUBLE_EQ(plus(-10, -10), -20);
}
////////TESTS FOR PLUS FUNCTION - END////////
//
//
////////TESTS FOR POWER FUNCTION - START////////
TEST (power, negbase_realexp) {
    //negative base cannot have non-integer exponent, returns 'not a number'
    EXPECT_TRUE(std::isnan(power(-2, 2.15)));
    EXPECT_TRUE(std::isnan(power(-5.7, 2.18)));
}
TEST (power, negbase_intexp) {
    //negative base with integer exponent should not return 'not a number'
    EXPECT_FALSE(std::isnan(power(-2, 2 )));
    EXPECT_FALSE(std::isnan(power(-5.7, 2)));
}
TEST (power, negbase_evenexp) {
    //negative base with even exponent should return value greater than zero
    EXPECT_GT(power(-2, 2), 0);
    EXPECT_GT(power(-128, 6), 0);
}
TEST (power, negbase_oddexp) {
    //negative base with odd exponent should return value lesser than zero
    EXPECT_LT(power(-2, 3), 0);
    EXPECT_LT(power(-600, 13), 0);
}
TEST (power, infinity) {
    //infinite base with zero exp is undefined, returns 'nan', same for nan exp
    EXPECT_TRUE(std::isnan(power(INFINITY, 0)));
    EXPECT_TRUE(std::isnan(power(INFINITY, NAN)));
    //zero base with infinity exponent is 0      
    EXPECT_DOUBLE_EQ(power(0, INFINITY), 0);
    //positive base with infinity exponent is infinity
    EXPECT_DOUBLE_EQ(power(5, INFINITY), INFINITY);
    //negative base with infinity exponent is negative infinity
    EXPECT_DOUBLE_EQ(power(-5, INFINITY), -INFINITY);
    //infinity base with positive exponent
    EXPECT_DOUBLE_EQ(power(INFINITY, 5), INFINITY);
}
//now testing returned values
TEST (power, received_values) {
    //positive base && positive exp
    EXPECT_DOUBLE_EQ(power(0, 2), 0);
    EXPECT_DOUBLE_EQ(power(10, 0), 1);
    EXPECT_DOUBLE_EQ(power(10, 1), 10);
    EXPECT_DOUBLE_EQ(power(10, 2), 100);
    EXPECT_DOUBLE_EQ(power(2, 3), 8);
    //positive base && negative exp
    EXPECT_DOUBLE_EQ(power(10, -1), 0.1);
    EXPECT_DOUBLE_EQ(power(10, -2), 0.01);
    EXPECT_DOUBLE_EQ(power(2, -3), 0.125);
    //negative base && positive exp
    EXPECT_DOUBLE_EQ(power(-10, 1), -10);
    EXPECT_DOUBLE_EQ(power(-10, 2), 100);
    EXPECT_DOUBLE_EQ(power(-2, 3), -8);
    //negative base && negative exp
    EXPECT_DOUBLE_EQ(power(-2, -1), -0.5);
    EXPECT_DOUBLE_EQ(power(-2, -2), 0.25);
    EXPECT_DOUBLE_EQ(power(-4, -2), 0.0625);
}
////////TESTS FOR POWER FUNCTION - END////////

//tests for multiplication
TEST (multiplication, zero) {
    EXPECT_DOUBLE_EQ(multiplication(5, 0), 0);
    EXPECT_DOUBLE_EQ(multiplication(0, 5), 0);
    EXPECT_DOUBLE_EQ(multiplication(0, 0), 0);
}

TEST (multiplication, infinity){
    EXPECT_DOUBLE_EQ(multiplication(5, INFINITY), INFINITY);
    EXPECT_DOUBLE_EQ(multiplication(INFINITY, 5), INFINITY);
    EXPECT_DOUBLE_EQ(multiplication(INFINITY, INFINITY), INFINITY);
}

TEST (multiplication, not_a_number) {
    EXPECT_TRUE(std::isnan(multiplication(NAN, 5)));
    EXPECT_TRUE(std::isnan(multiplication(5, NAN)));
}

TEST (multiplication, positive_numbers) {
    EXPECT_DOUBLE_EQ(multiplication(2, 5), 10);
    EXPECT_DOUBLE_EQ(multiplication(10, 5), 50);
    EXPECT_DOUBLE_EQ(multiplication(10, 10), 100);
}
TEST (multiplication, negative_numbers) {
    EXPECT_DOUBLE_EQ(multiplication(-2, 5), -10);
    EXPECT_DOUBLE_EQ(multiplication(10, -5), -50);
    EXPECT_DOUBLE_EQ(multiplication(-10, -10), 100);
}


//tests for divide


TEST (division, zero) {
    EXPECT_TRUE(std::isnan(division(5, 0)));
    EXPECT_DOUBLE_EQ(division(0, 5), 0);
    EXPECT_TRUE(std::isnan(division(0, 0)));
}

TEST (division, infinity) {
    EXPECT_DOUBLE_EQ(division(5, INFINITY), 0);
    EXPECT_DOUBLE_EQ(division(INFINITY, 5), INFINITY);
    EXPECT_TRUE(std::isnan(division(INFINITY, INFINITY)));
}

TEST (division, not_a_number) {
    EXPECT_TRUE(std::isnan(division(NAN, 5)));
    EXPECT_TRUE(std::isnan(division(5, NAN)));
}

TEST (division, positive_numbers) {
    EXPECT_DOUBLE_EQ(division(6, 2), 3);
    EXPECT_DOUBLE_EQ(division(10, 5), 2);
    EXPECT_DOUBLE_EQ(division(10, 10), 1);
}

TEST (division, negative_numbers) {
    EXPECT_DOUBLE_EQ(division(-10, 5), -2);
    EXPECT_DOUBLE_EQ(division(10, -5), -2);
    EXPECT_DOUBLE_EQ(division(-10, -10), 1);
}



//tests for root

TEST (root, zero) {
    EXPECT_TRUE(std::isnan(root(0, 27)));
    EXPECT_DOUBLE_EQ(root(27, 0), 0);
    EXPECT_TRUE(std::isnan(root(0, 0)));
}

TEST (root, infinity) {
    EXPECT_DOUBLE_EQ(root(INFINITY, INFINITY), 1);

}

TEST (root, not_a_number) {
    EXPECT_TRUE(std::isnan(root(NAN, 5)));
    EXPECT_TRUE(std::isnan(root(5, NAN)));
}


TEST (root, positive_numbers) {
    double epsilon = 1e-10;

    EXPECT_NEAR(root(3, 27), 3, epsilon);
    EXPECT_NEAR(root(2, 4), 2, epsilon);
    EXPECT_NEAR(root(3, 512), 8, epsilon);
}

TEST (root, negative_numbers) {
    double epsilon = 1e-10;
    
    EXPECT_NEAR(root(-2, 25), 0.2, epsilon);
    EXPECT_TRUE(std::isnan(root(3, -27)));
}


//tests for sinus

TEST (sine, zero) {
    double epsilon = 1e-10;

    EXPECT_NEAR(sine(0), 0, epsilon);
}


TEST (sine, infinity) {
    EXPECT_TRUE(std::isnan(sine(INFINITY)));
}

TEST (sine, not_a_number) {
    EXPECT_TRUE(std::isnan(sine(NAN)));
}

TEST (sine, positive_numbers) {
    double epsilon = 1e-10;

    //check result with epsilon precision
    EXPECT_NEAR(sine(360), 0, epsilon);
    EXPECT_NEAR(sine(270), -1, epsilon);
    EXPECT_NEAR(sine(180), 0, epsilon);
    EXPECT_NEAR(sine(90), 1, epsilon);
}


TEST (sine, negative_numbers) {
    double epsilon = 1e-10;

    //check result with epsilon precision
    EXPECT_NEAR(sine(-360), 0, epsilon);
    EXPECT_NEAR(sine(-270), 1, epsilon);
    EXPECT_NEAR(sine(-180), 0, epsilon);
    EXPECT_NEAR(sine(-90), -1, epsilon);
}


//tests for cosinus

TEST (cosine, zero) {
    EXPECT_DOUBLE_EQ(cosine(0), 1);
}

TEST (cosine, infinity) {
    EXPECT_TRUE(std::isnan(cosine(INFINITY)));
}

TEST (cosine, not_a_number) {
    EXPECT_TRUE(std::isnan(cosine(NAN)));
}

TEST (cosine, positive_numbers) {
    double epsilon = 1e-10;

    //check result with epsilon precision
    EXPECT_NEAR(cosine(360), 1, epsilon);
    EXPECT_NEAR(cosine(270), 0, epsilon);
    EXPECT_NEAR(cosine(180), -1, epsilon);
    EXPECT_NEAR(cosine(90), 0, epsilon);
}

TEST (cosine, negative_numbers) {
    double epsilon = 1e-10;

    //check result with epsilon precision
    EXPECT_NEAR(cosine(-360), 1, epsilon);
    EXPECT_NEAR(cosine(-270), 0, epsilon);
    EXPECT_NEAR(cosine(-180), -1, epsilon);
    EXPECT_NEAR(cosine(-90), 0, epsilon);

}


