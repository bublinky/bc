/**
 * @file profiling.cpp
 * @date 2020-03-25
 
 * @author Roland Schulz (xschul06)
 
 * @brief Programm computing standard deviation, used for profiling of bcmaths
 *  library functions.
 *  run using:
 *  ./progprof < testdata.txt
 */

#include <iostream>
#include <string>

#include "bcmaths.h"

int main(int argc, char** argv) {
    double stdDeviation = 0;
    double averageSum = 0;
    double squaredSum = 0;
    double average = 0;
    std::string line;
    // count of numbers supplied
    int N = 0;
    double num;

    while (std::getline(std::cin, line)) {
        // check whether string to double conversion succeeds
        try {
            num = std::stod(line, nullptr);
        }catch (const std::exception& e) {
            std::cerr << argv[0] << ": " << e.what() << '\n';
            return 1;
        }
        // xi in average sum formula
        averageSum = plus(averageSum, num);
        // xi^2 in standard deviation formula
        squaredSum = plus(squaredSum, multiplication(num, num));
        N = plus(N, 1);
    }
    
    // check for division by zero
    // N <= 1 because std deviation formula contains 1/(N-1)
    if (N <= 1) {
        std::cerr << argv[0] << ": " << "Division by zero" << '\n';
        return 1;
    }

    average = division(averageSum, N);
    // = sqrt((1/(N-1)) * (squaredSum - (N*(average*average))))
    stdDeviation = root(2, multiplication(division(1, minus(N, 1)), minus(squaredSum, multiplication(N, multiplication(average, average)))));
    
    std::cout << stdDeviation << std::endl;

    return 0;
}
