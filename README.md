# Bubblecalc ![License](https://img.shields.io/badge/license-GPLv3-blue)

![screenshot](screenshot.png)

## Intro
**GUI calculator written in C++ using Qt**

University group [assignment](http://ivs.fit.vutbr.cz/projekt-2_tymova_spoluprace2019-20.html) for [IVS class](https://www.fit.vut.cz/study/course/13372/.cs) at [Brno, University of Technology, Faculty of Information Technology](https://www.fit.vut.cz/.en).


## Overview

|Branch|Description|[Pipeline](https://gitlab.com/bublinky/bc/pipelines)|
|:-:|:-:|:-:|
|[Master](https://gitlab.com/bublinky/bc/-/tree/master)|Stable releases|![Master pipeline badge](https://gitlab.com/bublinky/bc/badges/master/pipeline.svg?style=flat-square)
|[Develop](https://gitlab.com/bublinky/bc/-/tree/develop)|Bleeding edge|![Develop pipeline badge](https://gitlab.com/bublinky/bc/badges/develop/pipeline.svg?style=flat-square)

### Authors
@Bublinky work group:
- [Project lead] **Daniel Sedláček** (xsedla1p) - @DanielSed
- [Dev] **Roland Schulz** (xschul06) - @schullzroll
- [Dev] **Dominik Kroupa** (xkroup12) - @domkroup
- [Dev] **Rati Kereselidze** (xkeres01) - @Arturko

### Environment
This app is provided to run on **Ubuntu 18.04 64bit**. Tested against [official docker Ubuntu 18.04 image](https://hub.docker.com/_/ubuntu).

---
## Main repo contents

|Path|Description|
|:-|:-|
|[mockup](mockup)|Images of mockups for the next version of bc|
|[plan](plan)|Document containing task assignments for each author|
|[profiling](profiling)|Profiler output record and suggestions|
|[src](src)|Project sources|
|[debugging.png](debugging.png)|Screenshot of bcalc debugging workflow|
|[dokumentace.pdf](dokumentace.pdf)|Programm documentation|
|[screenshot.png](screenshot.png)|Screenshot of main calculator GUI|
|[skutecnost.txt](skutecnost.txt)|Document depicting the difference between [plan](plan/xsedla1p_xschul06_xkroup12_xkeres01_plan.txt) and reality|
|[hodnoceni.txt](hodnoceni.txt)|Personal score for each project member|
|[README.md](README.md)|You are reading this <3|
|[packages.txt](packages.txt)|Package dependencies for Ubuntu based systems|
|[LICENSE](LICENSE)|Project license|
---

## Table of contents

[[_TOC_]]

---
# Installation
## Dependencies
Packages required for running are listed in file [packages.txt](packages.txt) as lines, so it can be easily integrated in various other package managers. **Names of packages in [packages.txt](packages.txt) are intended & tested for Debian/Ubuntu apt package directories. Cross package manager compatibility is not guaranteed!**
### Exemplary installation of required dependencies using apt
```shell
cat packages.txt | xargs apt-get install -y
```

---

## Manual installation
### Make-manual
| Target         | Description|
|----------------|--------------------------------------------------------------------------------------------------------------------------------------|
| `make all`     | Compiles bcalc and profiling programm|
| `make run`     | Compiles bcalc and profiling programm + runs bcalc|
| `make pack`    | Archives this repo according to [assignement details](http://ivs.fit.vutbr.cz/projekt-2_tymova_spoluprace2019-20.html#sekceOdevzdani)|
| `make test`    | Compiles tests and runs them|
| `make doc`     | Creates project documentation|
| `make profile` | Compiles profiling programm and runs it|
| `make clean`   | Removes temporary files and build files|

Make target `make all` compiles main calculator app and profiling program, and puts the binaries in newly created `./build` directory.
### Git

```shell
git clone https://gitlab.com/bublinky/bc.git &&
cd bc/ &&
make all -C ./src/
```

If you plan on **running tests**, and haven't ran recursive git clone to dowload all submodules, run `make test` and git will automatically download [googletest library](https://github.com/google/googletest)
 submodule.

### Without Git
#### Download tar.gz and untar
```shell
wget https://gitlab.com/bublinky/bc/-/archive/master/bc-master.tar.gz &&
tar -zxvf bc-master.tar.gz &&
cd bc-master &&
make all -C ./src/
```
If you plan on **running tests**, you need to also download and unpack the [googletest library](https://github.com/google/googletest).

Navigate yourself to the repo base folder. And run:
```shell
wget https://github.com/google/googletest/archive/master.tar.gz -O ./src/lib/googletest/googletest.tar.gz &&
cd ./src/lib/googletest &&
tar -zxvf googletest.tar.gz &&
rm googletest.tar.gz &&
mv googletest-master/ googletest/ &&
cd ../../../
```
And then run tests using `make test`.

---

# License
This project is licensed under **GNU GPLv3**, see [LICENSE](LICENSE) for details.
