# Profiling report
You can see the output of profiling in vystup.txt file.
The profiling program calculates standard deviation.
In our report we ran the program with 10, 100 and 1000 test input values (which you can find in ../src/profiling/*.txt) and accumulate the results into one report.

# Report summary
Program spends the most time invoking functions plus and multiplication as per call count. Plus is called 3x and multiplication 1x for each input number.

# Optimization
When optimizing the code in the future, we should focus on minimizing the overhead that inevitably comes with using plus function to compute the sum (and multiplication for squared sum) of numbers, but due to unknown count of input numbers this task isn't at all trivial and would probably involve abandoning stream processing of input data. 
Merging multiplication and plus functions to one function.
Using variable length array data structure with Sum and SquaredSum functions could lead to compute time optimization at the cost of memory allocation delay and memory usage.

